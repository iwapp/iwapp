
jQuery.noConflict();
jQuery(document).mouseup(
	function (e){
		var container = jQuery("#sectionIframe");
        var btn1 = jQuery("#btnPerfil");
        var btn2 = jQuery("#btnReporte");
        //console.info(e.target);
		if (!container.is(e.target) && container.has(e.target).length === 0 ) // if the target of the click isn't the container nor a descendant of the container
		{
			jQuery(container).removeClass("show");
			jQuery(container).addClass("hidden");
            jQuery('#iframeContenet').attr('src','');
		}else if(jQuery(container).hasClass("show")){
			jQuery(container).removeClass("show");
			jQuery(container).addClass("hidden");
            jQuery('#iframeContenet').attr('src','');
        }else{
			jQuery(container).removeClass("hidden");
			jQuery(container).addClass("show");
		}
});

function openProfile(){
    jQuery('#iframeContenet').attr('src','views/profile.html');
    jQuery('#sectionIframe').removeClass('hidden');
}
function openReport(){
    jQuery('#iframeContenet').attr('src','views/report.html');
    jQuery('#sectionIframe').removeClass('hidden');
}